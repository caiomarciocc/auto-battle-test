﻿using System;
using System.Collections.Generic;
using System.Linq;
using static AutoBattle.Types;

namespace AutoBattle
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            var battlefield = new Grid(5, 5);
            Character playerCharacter;
            Character enemyCharacter;
            var allPlayers = new List<Character>();
            var currentTurn = 0;
            var availableNumbers = new List<int>() {1, 2, 3, 4};

            Setup();

            // Initiates the program
            void Setup()
            {
                GetPlayerChoice();
            }
            
            // Asks for the player to choose between for possible classes via console.
            void GetPlayerChoice()
            {
                Console.WriteLine("Choose Between One of this Classes:\n");
                Console.WriteLine("[1] Paladin, [2] Warrior, [3] Cleric, [4] Archer");
                
                // Store the player choice in a variable
                var choice = Console.ReadLine();

                // Change "choice" into a int value
                if (!string.IsNullOrEmpty(choice) && Char.IsNumber(choice, 0))
                {
                    var intChoice = int.Parse(choice);

                    // Check if the number typed is one of the available
                    if (availableNumbers.Contains(intChoice)) 
                        CreatePlayerCharacter(intChoice);
                    else
                        GetPlayerChoice();
                }
                
                else
                    GetPlayerChoice();
            }

            // Creating the Player Character using the given information
            void CreatePlayerCharacter(int classIndex)
            {
                var characterClass = (CharacterClass) classIndex;
                
                Console.WriteLine($"Player Class: {characterClass}");
                playerCharacter = new Character(characterClass, "Player");
                battlefield.player = playerCharacter;

                CreateEnemyCharacter();
            }

            // Creating the Enemy Character using the given information
            void CreateEnemyCharacter()
            {
                // Randomly choose the enemy class
                var enemyClass = (CharacterClass) GetRandomInt(1, 4);
                
                Console.WriteLine($"Enemy Class: {enemyClass}\n");
                enemyCharacter = new Character(enemyClass, "Enemy");
                
                StartGame();
            }
            
            // Setting the players targets
            void StartGame()
            {
                // Populates the character variables and targets
                enemyCharacter.target = playerCharacter;
                playerCharacter.target = enemyCharacter;
                allPlayers.Add(playerCharacter);
                allPlayers.Add(enemyCharacter);
                AllocatePlayers();
            }
            
            // Now its time to allocate the players on the grid
            void AllocatePlayers()
            {
                AllocateCharacter(playerCharacter);
                AllocateCharacter(enemyCharacter);
                
                battlefield.DrawBattlefield(battlefield);
                StartTurn();
            }

            // It chooses a random position on the grid for the character 
            void AllocateCharacter(Character character)
            {
                //var random = GetRandomInt(0, battlefield.grids.Count);
                var randomX = GetRandomInt(0, battlefield.xLength);
                var randomY = GetRandomInt(0, battlefield.yLength);
                
                //var randomLocation = (battlefield.grids.ElementAt(random));
                var randomLocation = battlefield.grids[randomX, randomY];
                Console.Write($"{character.name} Position: [{randomLocation.xIndex}, {randomLocation.yIndex}]\n");
                
                // Check if the random location is occupied. In case of TRUE, it repeats this function
                if (!randomLocation.isOccupied)
                {
                    randomLocation.isOccupied = true;
                    character.currentGrid = randomLocation;
                    battlefield.grids[randomX, randomY] = character.currentGrid;
                } 
                
                else
                    AllocateCharacter(character);
            }
            
            // It is responsible for making the players do their moves.
            void StartTurn()
            {
                // Only in the first turn, it randomizes who's the first player to start the actions
                if (currentTurn == 0)
                {
                    var random = new Random();
                    allPlayers = new List<Character>(allPlayers.OrderBy(x => random.Next()));
                }

                foreach (var character in allPlayers)
                {
                    character.StartTurn(battlefield);
                }

                currentTurn++;
                HandleTurn();
            }

            // Check if one of the player has died, otherwise it proceeds with the gameplay 
            void HandleTurn()
            {
                if ((playerCharacter.health <= 0) || (enemyCharacter.health <= 0))
                {
                    Console.Write(Environment.NewLine + Environment.NewLine);

                    Console.WriteLine(enemyCharacter.health <= 0 ? "YOU WON THE GAME!!!\n" : "YOU LOST THE GAME!!!\n");
                    Console.WriteLine("Click on any key to end game!\n");
                    var key = Console.ReadKey();

                    Console.Write(Environment.NewLine + Environment.NewLine);
                } 
                
                else
                {
                    Console.Write(Environment.NewLine + Environment.NewLine);
                    Console.WriteLine("Click on any key to start the next turn...\n");
                    Console.Write(Environment.NewLine + Environment.NewLine);

                    var key = Console.ReadKey();
                    StartTurn();
                }
            }
            
            // Return a random between the min and max numbers given
            int GetRandomInt(int min, int max)
            {
                var rand = new Random();
                var index = rand.Next(min, max);
                return index;
            }
        }
    }
}
