﻿using System;
using static AutoBattle.Types;

namespace AutoBattle
{
    public class Grid
    {
        #region Variables
        
        public GridBox[,] grids;
        public int xLength;
        public int yLength;
        public Character player;
        public Grid(int lines, int columns)
        {
            xLength = lines;
            yLength = columns;
            grids = new GridBox[lines, columns];
            Console.WriteLine("The battle field has been created\n");
            
            for (int i = 0; i < lines; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    GridBox newBox = new GridBox(i, j, false);
                    grids[i, j] = newBox;
                    Console.Write($"[ ]\t");
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }

        #endregion
        
        #region Methods

        /// <summary>
        /// Prints the matrix that indicates the tiles of the battlefield
        /// </summary>
        /// <param name="grid"></param>
        public void DrawBattlefield(Grid grid)
        {
            DrawBattlefield(grid.xLength, grid.yLength);
        }
        
        /// <summary>
        /// Prints the matrix that indicates the tiles of the battlefield
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="columns"></param>
        private void DrawBattlefield(int lines, int columns)
        {
            for (int i = 0; i < lines; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    var currentGrid = grids[j, i];
                    Console.Write(currentGrid.isOccupied ? GetGridPlayer(currentGrid) : $"[ ]\t");
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }

        /// <summary>
        /// Return "P" if the character is the "PLAYER" and "E" if its the "ENEMY"
        /// </summary>
        /// <param name="gridBox"></param>
        /// <returns></returns>
        private string GetGridPlayer(GridBox gridBox)
        {
            var checker = player.currentGrid.Equals(gridBox);
            return checker ? "[P]\t" : "[E]\t";
        }

        #endregion
    }
}