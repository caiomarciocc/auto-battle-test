﻿using System;
using System.Collections.Generic;
using static AutoBattle.Types;

namespace AutoBattle
{
    public class Character
    {
        #region Variables

        public string name;
        public int health;
        private int baseDamage;
        private int damageMultiplier;
        private bool isDead;
        public bool isInvulnerable;
        public GridBox currentGrid;
        public Character target { get; set; }
        private int currentRound;
        private CharacterClass characterClass;
        private CharacterClassSpecific characterClassSpecific;
        private CharacterSkills InvulnerableSkill = new CharacterSkills("Invulnerable", 20, 1, true);
        private CharacterSkills EnPowerSkill = new CharacterSkills("EnPower", 20, 2, false);
        private List<CharacterSkills> characterSkillsList = new List<CharacterSkills>();
        public Character (CharacterClass characterClass, string name)
        {
            Random random;
            int randomRoundCount;
            
            switch (characterClass)
            {
                case CharacterClass.Paladin:
                    this.name = $"{name} (Paladin)";
                    characterClass = CharacterClass.Paladin;
                    health = 100;
                    baseDamage = 15;
                    damageMultiplier = 1;
                    characterSkillsList.Add(InvulnerableSkill);
                    random = new Random();
                    randomRoundCount = random.Next(1, 4);
                    characterClassSpecific = new CharacterClassSpecific(randomRoundCount, health, baseDamage, characterSkillsList, CharacterClass.Paladin);
                    break;
                case CharacterClass.Warrior:
                    this.name = $"{name} (Warrior)";
                    characterClass = CharacterClass.Warrior;
                    health = 100;
                    baseDamage = 15;
                    damageMultiplier = 1;
                    characterSkillsList.Add(EnPowerSkill);
                    random = new Random();
                    randomRoundCount = random.Next(1, 4);
                    characterClassSpecific = new CharacterClassSpecific(randomRoundCount, health, baseDamage, characterSkillsList, CharacterClass.Warrior);
                    break;
                case CharacterClass.Cleric:
                    this.name = $"{name} (Cleric)";
                    characterClass = CharacterClass.Cleric;
                    health = 100;
                    baseDamage = 15;
                    damageMultiplier = 1;
                    characterSkillsList.Add(InvulnerableSkill);
                    random = new Random();
                    randomRoundCount = random.Next(1, 4);
                    characterClassSpecific = new CharacterClassSpecific(randomRoundCount, health, baseDamage, characterSkillsList, CharacterClass.Cleric);
                    break;
                case CharacterClass.Archer:
                    this.name = $"{name} (Archer)";
                    characterClass = CharacterClass.Archer;
                    health = 100;
                    baseDamage = 15;
                    damageMultiplier = 1;
                    characterSkillsList.Add(EnPowerSkill);
                    random = new Random();
                    randomRoundCount = random.Next(1, 4);
                    characterClassSpecific = new CharacterClassSpecific(randomRoundCount, health, baseDamage, characterSkillsList, CharacterClass.Archer);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(characterClass), characterClass, null);
            }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Check if a character its closer to a target, otherwise it will walk
        /// </summary>
        /// <param name="battlefield"></param>
        public void StartTurn(Grid battlefield)
        {
            CheckSkills();
            
            if (CheckCloseTargets(battlefield))
                Attack(target);
            else
                Walk(battlefield);

            currentRound++;
        }

        /// <summary>
        /// Checks if there are skills available for this character
        /// </summary>
        private void CheckSkills()
        {
            if (currentRound == 0)
            {
                foreach (var skill in characterSkillsList)
                {
                    baseDamage = skill._damage;
                    damageMultiplier = skill._damageMultiplier;
                    Console.WriteLine($"{name} has the {skill._name} skill!");
                }
                
                if (characterSkillsList.Contains(InvulnerableSkill))
                {
                    isInvulnerable = true;
                    Console.WriteLine($"{name} is invulnerable for {characterClassSpecific.skillRoundCount} rounds!");
                }
            }
            
            if (currentRound == characterClassSpecific.skillRoundCount)
            {
                baseDamage = 15;
                damageMultiplier = 1;
                isInvulnerable = false;
                   
                foreach (var skill in characterSkillsList)
                {
                    Console.WriteLine($"{name} has no longer the {skill._name} skill!");
                }
            }
        }

        /// <summary>
        /// Check in x and y directions if there is any character close enough to be a target.
        /// </summary>
        /// <param name="battlefield"></param>
        /// <returns></returns>
        private bool CheckCloseTargets(Grid battlefield)
        {
            bool left = !(IsOnLeftBorder()) && CheckXBorder(battlefield, currentGrid.xIndex - 1);
            bool right = !(IsOnRightBorder(battlefield)) && CheckXBorder(battlefield, currentGrid.xIndex + 1);
            bool up = !(IsOnUpperBorder()) && CheckYBorder(battlefield, currentGrid.yIndex - 1);
            bool down = !(IsOnLowerBorder(battlefield)) && CheckYBorder(battlefield, currentGrid.yIndex + 1);
            
            // It returns TRUE if at least one of the booleans were TRUE, otherwise it returns FALSE
            return left || right || up || down;
        }

        /// <summary>
        /// Check elements that are in the x border of the grid
        /// </summary>
        /// <returns></returns>
        private bool CheckXBorder(Grid battlefield, int xIndex)
        {
            if (xIndex >= battlefield.xLength || xIndex < 0)
                return false;
            return battlefield.grids[xIndex, currentGrid.yIndex].isOccupied;
        }
        
        /// <summary>
        /// Check elements that are in the y border of the grid
        /// </summary>
        /// <returns></returns>
        private bool CheckYBorder(Grid battlefield, int yIndex)
        {
            if (yIndex >= battlefield.yLength || yIndex < 0)
                return false;
            return battlefield.grids[currentGrid.xIndex, yIndex].isOccupied;
        }

        /// <summary>
        /// Attacks the target if you are not dead
        /// </summary>
        /// <param name="targetCharacter"></param>
        private void Attack(Character targetCharacter)
        {
            var rand = new Random();
            var randomDamage = rand.Next(0, baseDamage);
            var damageAmount = randomDamage * damageMultiplier;

            if (!isDead)
                targetCharacter.TakeDamage(damageAmount);
        }

        /// <summary>
        /// Responsible for the damage that the character takes
        /// </summary>
        /// <param name="damageAmount"></param>
        /// <returns></returns>
        private void TakeDamage(int damageAmount)
        {
            if (!isDead)
            {
                if (!(isInvulnerable))
                {
                    if (damageAmount == baseDamage)
                        Console.WriteLine($"{target.name} CRITICAL HIT!");
                    if (damageAmount == 0)
                        Console.WriteLine($"{target.name} MISSED!");

                    health -= damageAmount;
                
                    Console.WriteLine($"{target.name} attacked the {name} and did {damageAmount} damage\n");
                    Console.WriteLine($"{name}: HEALTH: {health}\n");
                
                    if (health <= 0)
                        Die();
                }

                else
                    Console.WriteLine($"{name} is Invulnerable for this round and did not took damage!\n");
            }
        }
        
        /// <summary>
        /// Turns the variable "isDead" to TRUE and prints a "die" message
        /// </summary>
        private void Die()
        {
            isDead = true;
            Console.WriteLine($"{name} has died!\n");
        }
        
        /// <summary>
        /// If calculates in which direction this character should move to be closer to a possible target
        /// </summary>
        /// <param name="battlefield"></param>
        private void Walk(Grid battlefield)
        {
            // If possible, the character goes left
            if (currentGrid.xIndex > target.currentGrid.xIndex)
            {
                if (!(IsOnLeftBorder()))
                {
                    WalkTo(battlefield, currentGrid.xIndex - 1, currentGrid.yIndex, "left");
                    return;
                }
            } 
                
            // If possible, the character goes right
            if (currentGrid.xIndex < target.currentGrid.xIndex)
            {
                if (!(IsOnRightBorder(battlefield)))
                {
                    WalkTo(battlefield, currentGrid.xIndex + 1, currentGrid.yIndex, "right");
                    return;
                }
            }

            // If possible, the character goes up
            if (currentGrid.yIndex > target.currentGrid.yIndex)
            {
                if (!(IsOnUpperBorder()))
                {
                    WalkTo(battlefield, currentGrid.xIndex, currentGrid.yIndex - 1, "up");
                    return;
                }
            }
                
            // If possible, the character will goe left
            if (currentGrid.yIndex < target.currentGrid.yIndex)
            {
                if (!(IsOnLowerBorder(battlefield)))
                {
                    WalkTo(battlefield, currentGrid.xIndex, currentGrid.yIndex + 1, "down");
                    return;
                }
            }

            // If none of the possibilities above where true, the character stays in place
            else
            {
                WalkTo(battlefield, currentGrid.xIndex, currentGrid.yIndex, "nothing");
            }
        }

        /// <summary>
        /// Changes the Character's CurrentGrid and prints the grid
        /// </summary>
        /// <param name="battlefield"></param>
        /// <param name="xIndex"></param>
        /// <param name="yIndex"></param>
        /// <param name="directionText"></param>
        private void WalkTo(Grid battlefield, int xIndex, int yIndex, string directionText)
        {
            currentGrid.isOccupied = false;
            battlefield.grids[currentGrid.xIndex, currentGrid.yIndex] = currentGrid;
            currentGrid = battlefield.grids[xIndex, yIndex];
            currentGrid.isOccupied = true;
            battlefield.grids[currentGrid.xIndex, currentGrid.yIndex] = currentGrid;
            Console.WriteLine($"{name} Walked {directionText}\n");
            battlefield.DrawBattlefield(battlefield);
        }
        
        /// <summary>
        /// Checks if the player is on the left border of the grid
        /// </summary>
        /// <returns></returns>
        private bool IsOnLeftBorder()
        {
            var isOnLeftBorder = currentGrid.xIndex == 0;
            return isOnLeftBorder;
        }

        /// <summary>
        /// Checks if the player is on the right border of the grid
        /// </summary>
        /// <param name="battlefield"></param>
        /// <returns></returns>
        private bool IsOnRightBorder(Grid battlefield)
        {
            var isOnRightBorder = currentGrid.xIndex == battlefield.xLength - 1;
            return isOnRightBorder;
        }
        
        /// <summary>
        /// Checks if the player is on the upper border of the grid
        /// </summary>
        /// <returns></returns>
        private bool IsOnUpperBorder()
        {
            var isOnUpperBorder = currentGrid.yIndex == 0;
            return isOnUpperBorder;
        }
        
        /// <summary>
        /// Checks if the player is on the lower border of the grid
        /// </summary>
        /// <param name="battlefield"></param>
        /// <returns></returns>
        private bool IsOnLowerBorder(Grid battlefield)
        {
            var isOnLowerBorder = currentGrid.yIndex == battlefield.yLength - 1;
            return isOnLowerBorder;
        }

        #endregion
    }
}
