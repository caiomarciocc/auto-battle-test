﻿using System.Collections.Generic;

namespace AutoBattle
{
    public static class Types
    {
        public struct CharacterClassSpecific
        {
            public int skillRoundCount;
            private int hpModifier;
            private int classDamage;
            private List<CharacterSkills> skills;
            private CharacterClass characterClass;
            
            public CharacterClassSpecific(int skillRoundCount, int hpModifier, int classDamage, List<CharacterSkills> skills, CharacterClass characterClass)
            {
                this.skillRoundCount = skillRoundCount;
                this.hpModifier = hpModifier;
                this.classDamage = classDamage;
                this.skills = skills;
                this.characterClass = characterClass;
            }
        }

        public struct GridBox
        {
            public int xIndex;
            public int yIndex;
            public bool isOccupied;

            public GridBox(int x, int y, bool isOccupied)
            {
                xIndex = x;
                yIndex = y;
                this.isOccupied = isOccupied;
            }
        }

        public struct CharacterSkills
        {
            public string _name;
            public int _damage;
            public int _damageMultiplier;
            private bool _isInvulnerable;

            public CharacterSkills(string name, int damage, int damageMultiplier, bool isInvulnerable)
            {
                _name = name;
                _damage = damage;
                _damageMultiplier = damageMultiplier;
                _isInvulnerable = isInvulnerable;
            }
        }

        public enum CharacterClass : uint
        {
            Paladin = 1,
            Warrior = 2,
            Cleric = 3,
            Archer = 4
        }
    }
}
